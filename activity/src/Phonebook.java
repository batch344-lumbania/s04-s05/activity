import java.util.ArrayList;
import java.util.List;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook() {
        this.contacts = new ArrayList<Contact>();
    }

    public Phonebook(List<Contact> list) {
        this.contacts = new ArrayList<Contact>();
        for (Contact contact : list) {
            this.contacts.add(contact);
        }
    }

    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(Contact newContact) {
        boolean isExisting = false;
        int index = 0;

        for (Contact contact : this.contacts) {
            if (contact.getName().equals(newContact.getName())) {
                isExisting = true;
                index = this.contacts.indexOf(contact);
            }
        }
        if (isExisting) {
            String oldContactNumber = this.contacts.get(index).getContactNumber();
            String oldAddress = this.contacts.get(index).getAddress();

            this.contacts.get(index).setContactNumber(oldContactNumber + "\n" + newContact.getContactNumber());
            this.contacts.get(index).setAddress(oldAddress + "\n" + newContact.getAddress());
        } else {
            this.contacts.add(newContact);
        }
    }
}
