public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();
        Contact johnDoe1 = new Contact("John Doe", "+639152468596", "my home Quezon City");
        Contact johnDoe2 = new Contact("John Doe", "+639228547963", "my office in Makati City");

        phonebook.setContacts(johnDoe1);
        phonebook.setContacts(johnDoe2);

        Contact janeDoe1 = new Contact("Jane Doe", "+693162148573", "my home in Caloocan City");
        Contact janeDoe2 = new Contact("Jane Doe", "+639173698541", "my office in Pasay City");

        phonebook.setContacts(janeDoe1);
        phonebook.setContacts(janeDoe2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("------------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber());
                System.out.println("-----------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println(contact.getAddress());
                System.out.println("===================================");
            }
        }
    }
}